<div align="center">
 <img src="assets/hello_worldBackWhite.png" alt="img hello world">
</div>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[![Spotify](https://novatorem-marcosbarker.vercel.app/api/spotify)](https://open.spotify.com/user/marcos_barker) <!--[<img alt="GIF" height="130px" src="https://media.giphy.com/media/6iG7AvqmLXgTvay1dq/giphy.gif">](https://open.spotify.com/user/marcos_barker)<br>-->
<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->

[![Open Source Love png1](https://badges.frapsoft.com/os/v1/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/)    ![](https://komarev.com/ghpvc/?username=marcosbarker)    [![Waka Readme](https://github.com/marcosbarker/marcosbarker/actions/workflows/waka-readme.yml/badge.svg?branch=master)](https://github.com/marcosbarker/marcosbarker/actions/workflows/waka-readme.yml)    ![Lines of code](https://img.shields.io/badge/From%20Hello%20World%20I%27ve%20Written-67234%20lines%20of%20code-blue)
<a href="https://linktr.ee/marcos_barker">
<img height="130px" src="https://github-readme-stats.vercel.app/api?username=marcosbarker&hide=stars&hide_title=true&hide_border=true&show_icons=true&include_all_commits=true&count_private=true&line_height=21&text_color=000&icon_color=3AFC55&bg_color=0,c64dff,4dfcff,52fa5a&theme=graywhite" /><img height="130px" src="https://github-readme-stats.vercel.app/api/top-langs/?username=marcosbarker&hide_title=true&hide_border=true&hide=PLpgSQL,jupyter%20notebook&layout=compact&langs_count=7&exclude_repo=comp426,Redventures-Movie-Quotes&text_color=000&icon_color=fff&bg_color=0,52fa5a,ffc64d&theme=graywhite" />
</a></br>

<div align="center">
 <img height="100px" src="https://github-profile-trophy.vercel.app/?username=marcosbarker&theme=dracula&no-bg=false&no-frame=false&title=Commit&title=Issues&title=MultipleLang&title=PullRequest&title=Repositories">
</div>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[![GitHub Streak](https://github-readme-streak-stats.herokuapp.com?user=marcosbarker&theme=vue&hide_border=true&stroke=000000&ring=FF914C&fire=C254FF&currStreakNum=000000&sideNums=282A36&currStreakLabel=000000&sideLabels=000000&dates=282A36)](https://git.io/streak-stats)<br>

<!--START_SECTION:waka-->
**I'm an Early 🐤** 

```text
🌞 Morning    228 commits    ████░░░░░░░░░░░░░░░░░░░░░   15.78% 
🌆 Daytime    532 commits    █████████░░░░░░░░░░░░░░░░   36.82% 
🌃 Evening    434 commits    ███████░░░░░░░░░░░░░░░░░░   30.03% 
🌙 Night      251 commits    ████░░░░░░░░░░░░░░░░░░░░░   17.37%

```
📅 **I'm Most Productive on Friday** 

```text
Monday       219 commits    ███░░░░░░░░░░░░░░░░░░░░░░   15.16% 
Tuesday      198 commits    ███░░░░░░░░░░░░░░░░░░░░░░   13.7% 
Wednesday    234 commits    ████░░░░░░░░░░░░░░░░░░░░░   16.19% 
Thursday     202 commits    ███░░░░░░░░░░░░░░░░░░░░░░   13.98% 
Friday       295 commits    █████░░░░░░░░░░░░░░░░░░░░   20.42% 
Saturday     104 commits    █░░░░░░░░░░░░░░░░░░░░░░░░   7.2% 
Sunday       193 commits    ███░░░░░░░░░░░░░░░░░░░░░░   13.36%

```


📊 **This Week I Spent My Time On** 

```text
⌚︎ Time Zone: America/Sao_Paulo

🔥 Editors: 
VS Code                  1 hr 31 mins        █████████████████████████   100.0%

💻 Operating System: 
Windows                  58 mins             ████████████████░░░░░░░░░   63.95% 
Linux                    32 mins             █████████░░░░░░░░░░░░░░░░   36.05%

```


 Last Updated on 23/10/2021
<!--END_SECTION:waka-->
<a>
  <img width="800px" src="https://activity-graph.herokuapp.com/graph?username=marcosbarker&bg_color=ffffff&color=000000&line=3AFC55&point=c64dff&area=true&hide_border=true" />
</a>
<a href="https://github.com/marcosbarker/barkerDexPokeAPI">
  <img height="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=barkerDexPokeAPI&bg_color=0,3B93E6,4dfcff,3AFC55&theme=graywhite" />
</a>  
<a href="https://github.com/marcosbarker/serratec.residencia">  
  <img height="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=serratec.residencia&bg_color=0,3AFC55,52fa5a,52fa5a,ffc64d&theme=graywhite" />
</a>
<a href="https://github.com/marcosbarker/alura.imersaoDev">
  <img height="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=alura.imersaoDev&bg_color=0,3B93E6,4dfcff,3AFC55&theme=graywhite" />
</a>  
<a href="https://github.com/marcosbarker/alura.imersaoReact-Alurakut">  
  <img height="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=alura.imersaoReact-Alurakut&bg_color=0,3AFC55,52fa5a,ffc64d&theme=graywhite" />
</a>
<a href="https://github.com/marcosbarker/javaPOO">
  <img href="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=javaPOO&bg_color=0,3B93E6,4dfcff,4dfcff,4dfcff,3AFC55&theme=graywhite" />
</a>
<a href="https://github.com/marcosbarker/NLW4-rocketpay">  
  <img href="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=NLW4-rocketpay&bg_color=0,52fa5a,ffc64d&theme=graywhite" />
</a>
<a href="https://github.com/marcosbarker/instagram-clone-homepage">  
  <img href="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=instagram-clone-homepage&bg_color=3B93E6,3B93E6,4dfcff,52fa5a&theme=graywhite" />
</a>
<a href="https://github.com/marcosbarker/netflix-simple-copy">  
  <img href="140px" src="https://github-readme-stats.vercel.app/api/pin/?username=marcosbarker&repo=netflix-simple-copy&bg_color=0,52fa5a,ffc64d,ffc64d&theme=graywhite" />
</a>
<!--
#c64dff
#3AFC55
#52fa5a
#ffc64d
#3B93E6
#4dfcff
#ffffff
#9e4c98
#00e658
#df82f2
#000000
-->

* # EM TESTE 🧪🧪🧪🧪

Spotify Recently Played README Generator<br>
Markdown code snippet:

![Alt text](https://spotify-recently-played-readme.vercel.app/api?user=marcos_barker)

For custom count (1 ≤ {count} ≤ 10):

![Alt text](https://spotify-recently-played-readme.vercel.app/api?user=marcos_barker&count=3<={count}<=10)

For custom width (300 ≤ {width} ≤ 1000):

![Alt text](https://spotify-recently-played-readme.vercel.app/api?user=marcos_barker&width=450<={width}<=1000)



